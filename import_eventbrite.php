<?php
function jck_eventbrite(){
	
	global $wpdb;
	
	// This is the post type that you want to insert your events into.
	$event_posttype = "cpt_event";
	$apikey = "your-api-key-for-eventbrite";
	
	// These are the usernames for the eventbrite feeds you want to get. this allows for multiple feeds.
	$feeds = array(
		'email@domain.com',
		'another@domain.com'
	);
	
	foreach($feeds as $user):
	
		$completeurl = "https://www.eventbrite.com/xml/user_list_events?app_key={$apikey}&user={$user}&do_not_display=style";
		$xml = simplexml_load_file($completeurl);		
		$events = $xml->event;
		$totalevents = count($events);
		
		for ($i = 0; $i < $totalevents; $i++) {
			
			// d($events[$i]);
			
			$eventid = intval($events[$i]->id);																// Event ID
			$name = strval($events[$i]->title);																// Event Name			
			$eventurl = strval($events[$i]->url);															// Event URL
			$logourl = strval($events[$i]->logo);															// logo URL
			$description = strip_tags( strval($events[$i]->description), '<a><img>');						// Description
			$privacy = strval($events[$i]->privacy);														// Privacy of event
			$startdate = strval($events[$i]->start_date);													// Start Date
			$enddate = strval($events[$i]->end_date);														// End Date
				$startexplode = explode(' ', $startdate);
				$data_startdate = str_replace('-', '', $startexplode[0]);									// Start Date for Sorting
				$endexplode = explode(' ', $startdate);
				$data_enddate = str_replace('-', '', $endexplode[0]);										// End Date for Sorting
			
			$repeats = strval($events[$i]->repeats);														// Repeats? (yes/no)
				if($repeats == 'yes') { $repeat_schedule = strval($events[$i]->repeat_schedule); }			// Repeat Schedule
			// Organiser
			$organiser = $events[$i]->organizer;													// Organiser Object
				$organiser_name = strval($organiser->name);													// Organiser Name
				$organiser_url = strval($organiser->url);													// Organiser URL
				$organiser_desc = strip_tags( strval($organiser->description), '<a><img>');					// Organiser Description
				$organiser_longdec = strip_tags( strval($organiser->long_description), '<a><img>');			// Organiser Long Description
			// Venue
			$venue = $events[$i]->venue;															// Venue Object
				$venue_name = strval($venue->name);														    // Venue Name
				$address = array();
				$venue_latlon = strval($venue->{'Lat-Long'});											    // Lat/Lon
				if($venue->address != "") { $address[] = strval($venue->address);	}					   	// Address line 1
				if($venue->address_2 != "") { $address[] = strval($venue->address_2); }						// Address Line 2
				if($venue->city != "") { $address[] = strval($venue->city); }							 	// Venue City
				if($venue->region != "") { $address[] = strval($venue->region); }						   	// Venue Region
				if($venue->postal_code != "") { $address[] = strval($venue->postal_code);	}			    // Venue Postcode
				$venue_country = strval($venue->country);												    // Venue Country
				if($venue->country_code != "") { $address[] = strval($venue->country_code); }			   	// Venue Country Code
				
				$coords = str_replace(' / ', ',', $venue_latlon);
				$venueaddress = implode(', ', $address);
				
				$location = $venueaddress."|".$coords;
				
				
				
			// Tickets
			$tickets = $events[$i]->tickets->ticket;												// Tickets Object (currency/description/end_date/max/min/name/price/type/visible)
				$totaltickets = count($tickets);
				
				$alltickets = array();
				
				for ($ii = 0; $ii < $totaltickets; $ii++) {
					$alltickets[$ii]['currency'] = strval($tickets[$ii]->currency);
					$alltickets[$ii]['description'] = strip_tags( strval($tickets[$ii]->description), '<a><img>');
					$alltickets[$ii]['end_date'] = strval($tickets[$ii]->end_date);
					$alltickets[$ii]['ticket_id'] = strval($tickets[$ii]->id);
					$alltickets[$ii]['max'] = strval($tickets[$ii]->max);
					$alltickets[$ii]['min'] = strval($tickets[$ii]->min);
					$alltickets[$ii]['ticket_name'] = strval($tickets[$ii]->name);
					$alltickets[$ii]['price'] = strval($tickets[$ii]->price);
					$alltickets[$ii]['type'] = strval($tickets[$ii]->type);
					$alltickets[$ii]['visible'] = strval($tickets[$ii]->visible);
				}
			
			// Search for events with our eventid
			$querystr = "
				SELECT $wpdb->posts.* 
				FROM $wpdb->posts, $wpdb->postmeta
				WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 
				AND $wpdb->postmeta.meta_key = 'event_id' 
				AND $wpdb->postmeta.meta_value = '{$eventid}'
				AND $wpdb->posts.post_status = 'publish' 
				AND $wpdb->posts.post_type = '{$event_posttype}'
				ORDER BY $wpdb->posts.post_date DESC
			";
			
			$existingevents = $wpdb->get_results($querystr, OBJECT);
			
			if ($existingevents): // If post already exists (based on the evnt ID), update the values
			
				$existingid = $existingevents[0]->ID;
			
				$ignore = get_post_meta($existingid, 'ignore', true);
				
				if($ignore != 1) : // If this event has not been edited and set	to ignore auto updates, do this

					update_field('field_63', $eventid, $existingid); // Event ID
					update_field('field_64', $eventurl, $existingid); // URL
					update_field('field_65', $logourl, $existingid);	// Logo URL
					update_field('field_66', $privacy, $existingid);	// Privacy
					update_field('field_68', $startdate, $existingid);	// Start Date
					update_field('field_69', $enddate, $existingid);	// End Date
					if($repeats == 'yes') {
					update_field('field_70', $repeat_schedule, $existingid);	// Repeat Schedule
					}
					update_field('field_71', $organiser_name, $existingid);	// Organiser name
					update_field('field_72', $organiser_url, $existingid);	// Organiser URL
					update_field('field_73', $organiser_desc, $existingid);	// Organiser Desc
					update_field('field_74', $organiser_longdec, $existingid);	// Organiser Long Desc
					update_field('field_76', $venue_name, $existingid);	// Venue Name
					update_field('field_75', $location, $existingid);	// Venue Location
					update_field('field_77', $alltickets, $existingid);	// Tickets
					
					update_field('field_94', $data_startdate, $existingid);	// Start Date
					update_field('field_95', $data_enddate, $existingid);	// End Date
				
				endif;
				
			else: // If post doesn't exist, create it!
				
				// Create post object
				$newevent = array(
				  'post_title'    => $name,
				  'post_content'  => $description,
				  'post_status'   => 'publish',
				  'post_author'   => 1,
				  'post_type' => $event_posttype
				);
				
				// Insert the post into the database
				$newevent_id = wp_insert_post( $newevent );
				
				update_field('field_63', $eventid, $newevent_id); // Event ID
				update_field('field_64', $eventurl, $newevent_id); // URL
				update_field('field_65', $logourl, $newevent_id);	// Logo URL
				update_field('field_66', $privacy, $newevent_id);	// Privacy
				update_field('field_68', $startdate, $newevent_id);	// Start Date
				update_field('field_69', $enddate, $newevent_id);	// End Date
				if($repeats == 'yes') {
				update_field('field_70', $repeat_schedule, $newevent_id);	// Repeat Schedule
				}
				update_field('field_71', $organiser_name, $newevent_id);	// Organiser name
				update_field('field_72', $organiser_url, $newevent_id);	// Organiser URL
				update_field('field_73', $organiser_desc, $newevent_id);	// Organiser Desc
				update_field('field_74', $organiser_longdec, $newevent_id);	// Organiser Long Desc
				update_field('field_76', $venue_name, $newevent_id);	// Venue Name
				update_field('field_75', $location, $newevent_id);	// Venue Location
				update_field('field_77', $alltickets, $newevent_id);	// Tickets
				
				update_field('field_94', $data_startdate, $newevent_id);	// Start Date
				update_field('field_95', $data_enddate, $newevent_id);	// End Date
			
			endif;
		    
		}
	
	endforeach;

}
?>